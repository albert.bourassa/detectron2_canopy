import torch
from detectron2 import model_zoo
from detectron2.engine import DefaultPredictor
from detectron2.config import get_cfg
from detectron2.data import MetadataCatalog
from detectron2.utils.visualizer import ColorMode

class CFGModel():

    cfg = get_cfg()
    cfg_path = "COCO-PanopticSegmentation/panoptic_fpn_R_101_3x.yaml"
    config_path="COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x.yaml"

    def __init__(self):
        self.cfg.MODEL.DEVICE = "cpu"
        self.cfg.merge_from_file(model_zoo.get_config_file(self.cfg_path))
        self.cfg.MODEL.WEIGHTS = model_zoo.get_checkpoint_url(self.cfg_path)

        self.cpu_device = torch.device("cpu")
        self.metadata = MetadataCatalog.get(self.cfg.DATASETS.TRAIN[0]) 
        self.instance_mode = ColorMode.IMAGE
        self.predictor = DefaultPredictor(self.cfg)

        self.create_categories()
    
    def create_categories(self):
        self.green_idx = [self.cat_stuff().index("tree")]#, self.cat_stuff().index("flower"), self.cat_stuff().index("grass")]
        self.road_idx = [self.cat_stuff().index("road"), self.cat_stuff().index("pavement"), self.cat_stuff().index("dirt")]
        self.sky_idx = [self.cat_stuff().index("sky")]
        self.building_idx = [self.cat_stuff().index("building")]
        self.stuff_idx = {}
        self.stuff_idx['green'] = self.green_idx
        self.stuff_idx['road'] = self.road_idx
        self.stuff_idx['sky'] = self.sky_idx
        self.stuff_idx['building'] = self.building_idx
    
    def cat_things(self):
          return self.metadata.thing_classes
      
    def cat_stuff(self):
        return self.metadata.stuff_classes

    def model():
        return {'green':[], 'road':[], 'sky':[], 'building':[]}
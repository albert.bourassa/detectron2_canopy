import geopandas as gpd
import os
import cv2

def validate(path):
    f = all_files_present(path)
    return f

def all_files_present(path):
    dir_content = os.listdir(path)
    video = list(filter(lambda x: x.endswith(".mp4"), dir_content))

    files = []
    for x in video:
        files.append(x[:len(x)-4])
    
    return files
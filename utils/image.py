#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 15 10:24:51 2021

@author: david
"""

#%%
import torch
import cv2
import matplotlib.pyplot as plt

from detectron2 import model_zoo
from detectron2.engine import DefaultPredictor
from detectron2.config import get_cfg
from detectron2.utils.video_visualizer import VideoVisualizer
from detectron2.utils.visualizer import ColorMode, Visualizer
from detectron2.data import MetadataCatalog

#%%

#%%
class ImageUtils:
    def image(path):
        image = cv2.imread(path)
        return cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        
    def drawBox(image, bbox, color=(255,0,0)):
        pt1 = (bbox[0], bbox[1])
        pt2 = (bbox[2], bbox[3])
        cv2.rectangle(image, pt1, pt2, color, 2, 1)
        
    def drawClass(image, bbox, text, color=(255,0,0)):
        ft_l = len(text) * 20
        pt1 = (bbox[0], bbox[1] - 30)
        pt2 = (bbox[0] + ft_l, bbox[1])
        pt_text = (bbox[0] +2, bbox[1] - 10)
        cv2.rectangle(image, pt1, pt2, color, -1)
        cv2.putText(image, text, pt_text, cv2.FONT_HERSHEY_SIMPLEX, 1, (255,255,255), 2)
        
        
    def drawDistance(image, bbox, text, color=(255,0,0)):
        ft_l = len(text) * 12
        pt1 = (bbox[0], bbox[3] - 30)
        pt2 = (bbox[0] + ft_l, bbox[3])
        pt_text = (bbox[0] +2, bbox[3] - 10)
        cv2.rectangle(image, pt1, pt2, color, -1)
        cv2.putText(image, text, pt_text, cv2.FONT_HERSHEY_SIMPLEX, .5, (255,255,255), 1)
        
    def read_image(path):
        image = cv2.imread(path)
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        
        return image
        
    def show(path):
        image = cv2.imread(path)
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        plt.figure(figsize=(20,10))
        plt.imshow(image)
        plt.axis('off')
        
    def show_panoptic(path, image):
        plt.figure(figsize=(20,10))
        plt.subplot(121)
        plt.imshow(ImageUtils.read_image(path))
        plt.axis('off')
        plt.subplot(122)
        plt.imshow(image)
        plt.axis('off')
        
        plt.show()
        
    def save(image, path):
        cv2.imwrite(path, image)
#%%

#%%
class VisualisationImage:
    def __init__(self, cfg, config_path="COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x.yaml"):
      cfg.MODEL.DEVICE = "cpu"
      cfg.merge_from_file(model_zoo.get_config_file(config_path))
      cfg.MODEL.WEIGHTS = model_zoo.get_checkpoint_url(config_path)
      
      self.cpu_device = torch.device("cpu")
      self.metadata = MetadataCatalog.get(cfg.DATASETS.TRAIN[0]) 
      self.instance_mode = ColorMode.IMAGE
      self.predictor = DefaultPredictor(cfg) 
      
    def cat_things(self):
          return self.metadata.thing_classes
      
    def cat_stuff(self):
        return self.metadata.stuff_classes
      
    def run(self, image_path, min_score = .8):
        img = cv2.imread(image_path)
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        
        predictions = self.predictor(img)
        instances = predictions["instances"]
        instances = instances[instances.scores > min_score]
        
        v = Visualizer(img[:,:,::-1], self.metadata, scale=1.2)
        out = v.draw_instance_predictions(instances.to(self.cpu_device))
        
        
        return predictions, out
    
    def run_distance(self, image_path, min_score= .8):
        img = cv2.imread(image_path)
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        predictions = self.predictor(img)
        instances = predictions["instances"]
        instances = instances[instances.scores > min_score]
        
        return instances, img
        
        
    def run_panoptic_seg(self, image_path, min_score=.8):
        img = cv2.imread(image_path)
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        
        predictions = self.predictor(img)
        panoptic_seg, segments_info = predictions['panoptic_seg']
        #segments_info = segments_info[segments_info.scores > .9]
        
        v = Visualizer(img[:,:,::-1], self.metadata, scale=1.2)
        out = v.draw_panoptic_seg_predictions(panoptic_seg.to(self.cpu_device), segments_info)
        
        return panoptic_seg, segments_info, out
    
    def run_panoptic_seg_tree(self, image_path, min_score=.8):
        img = cv2.imread(image_path)
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        
        predictions = self.predictor(img)
        panoptic_seg, segments_info = predictions['panoptic_seg']
        
        segments_info = [i for i in segments_info if i['category_id'] == 37]
        
        
        v = Visualizer(img[:,:,::-1], self.metadata, scale=1.2)
        out = v.draw_panoptic_seg_predictions(panoptic_seg.to(self.cpu_device), segments_info)
    
        
        return panoptic_seg, segments_info, out

#%%



#%%

def show_result(original, result):
    rows = 2
    columns = 1
    fig = plt.figure(figsize=[18,18])
    
    fig.add_subplot(rows, columns, 1)
    plt.imshow(original) 
    plt.axis('off')
    plt.title("Original")
    fig.add_subplot(rows, columns, 2)
    plt.imshow(result[:, :, ::-1])
    plt.title("Result")
    plt.axis('off')
    
    
def save_result(original, result, path):
    show_result(original, result)
    plt.savefig(path)
    
#%%


#%%
if __name__ == "__main__":
    import os
    import cv2
    import matplotlib.pyplot as plt
    from detectron2.data.detection_utils import read_image
    cur_dir = os.path.dirname(os.path.realpath(__file__))
    
    image_path = os.path.join(cur_dir, "..", "images", "img_4.png")
    target_path = image_path.replace(".png", "_test.png")
    
    img = cv2.imread(image_path)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    #plt.imshow(img)
    #plt.axis('off')
    
    
    cfg = get_cfg()
    visualizer = VisualisationImage(cfg)

    predictions, output = visualizer.run(image_path)
    #cv2.imwrite(filename, img)
    #plt.imshow(output.get_image()[:, :, ::-1])
    
    save_result(img, output.get_image(), target_path)
    
    

#%%
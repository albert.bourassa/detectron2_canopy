1. Install requirements: pip install -r requirements.txt
    1.1 if problem with detectron2, comment line in requirements.txt and run alternate pip command
    1.2 if problem with geopandas, run:
        pip install wheel
        pip install pipwin

        pipwin install numpy
        pipwin install pandas
        pipwin install shapely
        pipwin install gdal
        pipwin install fiona
        pipwin install pyproj
        pipwin install six
        pipwin install rtree
        pipwin install geopandas

2. Place video you want to test in ./sample/ as well as matching .gpkg and .shp, all having the same name (example already in folder)

3. Run main and wait for results in ./sample/target/
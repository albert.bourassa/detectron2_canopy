#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 15 09:05:53 2021

@author: david
Modifié par Albert

Few commands to manipulate video files
ffmpeg -i video.mp4 -vcodec copy -acodec copy -ss 00:20:00.000 -to 00:21:00.000 sample_1.mp4
ffmpeg -i input.mp4 -vf scale=320:-1 output.mp4
ffmpeg -ss 00:46:18 -i video.mp4 -vframes 1 -q:v 2 ../images/img_5.png
"""

#%%
from itertools import chain
import torch

from detectron2 import model_zoo
from detectron2.engine import DefaultPredictor
from detectron2.config import get_cfg
from detectron2.utils.video_visualizer import VideoVisualizer
from detectron2.utils.visualizer import ColorMode, Visualizer
from detectron2.data import MetadataCatalog

import cv2

from cfg_model import CFGModel
#%%

#%%
class VideoWrapper:
  def __init__(self, path, fpm = 0):
    self.video = cv2.VideoCapture(path)
    self.frame_rate = int(round(self.video.get(cv2.CAP_PROP_FPS), 0))
    self.fpm = fpm if fpm != 0 else self.frame_rate * 60
    self.frame_count = int(self.video.get(cv2.CAP_PROP_FRAME_COUNT))
    self.total_frames = int(self.frame_count / (self.frame_rate * 60 / self.fpm)) + 1
    self.fps = self.video.get(cv2.CAP_PROP_FPS)
    self.fourcc = cv2.VideoWriter_fourcc(*"mp4v")
    
  def frame_generator(self):
    while self.video.isOpened():
      success, frame = self.video.read()
      while success and (self.video.get(cv2.CAP_PROP_POS_FRAMES) - 1) % ((self.frame_rate * 60) / self.fpm):
        success, frame = self.video.read()
      if success:
        yield frame
      else:
        break
    
  def size(self):
    return int(self.video.get(cv2.CAP_PROP_FRAME_WIDTH)), int(self.video.get(cv2.CAP_PROP_FRAME_HEIGHT))

  def area(self):
      width, height = self.size()
      
      return width * height

  def draw_bbox(self, frame, bbox, color= (255,0,0)):
      pt1 = (bbox[0], bbox[1])
      pt2 = (bbox[2], bbox[3])
      cv2.rectangle(frame, pt1, pt2, color, 2, 2)

  def writer(self, file_path):
    return cv2.VideoWriter(
                filename=file_path,
                fourcc=self.fourcc,
                fps=float(self.fps),
                frameSize=self.size(),
                isColor=True,
            )

  # {'id': 10, 'isthing': False, 'category_id': 37, 'area': 83397.0}
  def writeSegment(self, frame, segment, scale=3):
      if segment is None:
          segment = {'area': 0, 'area_percent': 0}
          
      text = "{} {:.2f}".format(segment['area'], segment['area_percent'] * 100)
      cv2.putText(frame, text, (10, 30), cv2.FONT_HERSHEY_PLAIN, scale, (0,255,0), 2)

  def writeCategories(self, frame, segment, scale=3):
    if segment is None:
      segment['None'] = {'area': 0, 'area_percent': 0}

    i = 1
    for cat in segment:
      text = cat + ": {} {:.2f}".format(segment[cat]['area'], segment[cat]['area_percent'] * 100)
      cv2.putText(frame, text, (10, 30*i), cv2.FONT_HERSHEY_PLAIN, scale, (0,255,0), 2)
      i = i + 1

  def release(self):
      self.video.release()

#%%

#%%
class VisualisationVideo:

  def __init__(self, cfg_model : CFGModel):
    self.cfg_model = cfg_model

  # def run(self, wrapper: VideoWrapper):
  #   frame_gen = wrapper.frame_generator()
  #   video_visualizer = VideoVisualizer(self.metadata, self.instance_mode)
    
  #   for frame in frame_gen:
  #     frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
  #     predictions = self.predictor(frame)
  #     instances = predictions["instances"]
  #     instances = instances[instances.scores > .8]
  #     visual_frame = video_visualizer.draw_instance_predictions(frame, instances.to(self.cpu_device))

  #     visual_frame = cv2.cvtColor(visual_frame.get_image(),  cv2.COLOR_RGB2BGR)
  #     yield visual_frame

  def run_panoptic_cat(self, wrapper : VideoWrapper):
    frame_gen = wrapper.frame_generator()

    total_area = wrapper.area()

    for frame in frame_gen:
      v = Visualizer(frame[:,:,::-1], self.cfg_model.metadata, scale=1.2)
      frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
      predictions = self.cfg_model.predictor(frame)
      panoptic_seg, segments_info = predictions['panoptic_seg']
      ids = list(chain(x for x in self.cfg_model.stuff_idx for x in self.cfg_model.stuff_idx[x]))
      segments_info = list(filter(lambda x: x['category_id'] in ids, segments_info))
      instances = predictions["instances"]
      instances = instances[instances.scores > .8]

      pan_list = predictions['panoptic_seg'][1]
      result = {}
      for cat in self.cfg_model.stuff_idx:
        r = {}
        r['area'] = 0
        for c in self.cfg_model.stuff_idx[cat]:
          n = next((x for x in pan_list if x['category_id'] == c and x['isthing'] == False), None)
          if n is not None:
            r['area'] = r['area'] + n['area']
        r['area_percent'] = r['area'] / total_area
        result[cat] = r
        
      visual_frame = v.draw_panoptic_seg(panoptic_seg.to(self.cfg_model.cpu_device), segments_info)

      frame = cv2.cvtColor(visual_frame.get_image(), cv2.COLOR_RGB2BGR)
      
      yield frame, result      
      
  def run_predictions(self, wrapper):
    frame_gen = wrapper.frame_generator()
    video_visualizer = VideoVisualizer(self.cfg_model.metadata, self.cfg_model.instance_mode)
  
    for frame in frame_gen:
      frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
      predictions = self.predictor(frame)
      
      yield predictions, frame
#%%


#%%
if __name__ == "__main__":
    import os
    import tempfile
    import numpy as np
    from utils.video import VideoWrapper
    cur_dir = os.path.dirname(os.path.realpath(__file__))
    video_path = os.path.join(cur_dir, "..", "video", "sample_2_1.mp4" )
    wrapper = VideoWrapper(video_path)

    
    with tempfile.TemporaryDirectory(prefix="video_format_test") as dir:
        filename = os.path.join(dir, "test_file.mp4")
        writer = wrapper.writer(filename)
        [writer.write(np.zeros((10, 10, 3), np.uint8)) for _ in range(30)]
        writer.release()
        wrapper.release()
        
        if os.path.isfile(filename):
            print("Success")
        else:
            print("Error")
            
            
    cfg = get_cfg()
    visualizer = VisualisationVideo(cfg)
    
#%%

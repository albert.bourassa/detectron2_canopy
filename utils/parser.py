#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#%%
import os
from argparse import ArgumentParser, ArgumentTypeError


def get_seconds(start):
    return start.hour * 3600 + start.minute * 60 + start.second


def valid_time(s):
    from datetime import datetime
    try:
        print(s)
        return datetime.strptime(s, "%H:%M:%S")
    except ValueError:
        msg = "not a valid date: {0!r}".format(s)
        raise ArgumentTypeError(msg)

def get_video_parser() -> ArgumentParser:
    cur_dir = os.path.dirname(os.path.realpath(__file__))
    video_path = os.path.join(cur_dir, "..", "sample", "sample.mp4")
    parser = ArgumentParser(description="Detect tree from a video")
    parser.add_argument("--path", help="video path", default=video_path)
    parser.add_argument("--save", help="save video with percentage of tree coverage", default=False, type=bool)
    parser.add_argument("--fpm", help="analyze n frames per minute where n is the argument", default=0, type=int)

    return parser

def get_image_parser() -> ArgumentParser:
    cur_dir = os.path.dirname(os.path.realpath(__file__))
    img_default = os.path.join(cur_dir, "..", "sample", "image.png")
    parser = ArgumentParser(description="Detect tree from an image")
    parser.add_argument("--path", help="image path", default=img_default)
    
    return parser
#%%

#%%
if __name__ == "__main__":
    args = get_video_parser().parse_args()
    
    assert os.path.basename(args.path) == "sample.mp4"
    assert os.path.exists(args.path)
    
    args = get_image_parser().parse_args()
    
    assert os.path.basename(args.path) == "image.png"
    assert os.path.exists(args.path)
#%%30
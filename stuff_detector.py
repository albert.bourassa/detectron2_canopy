from msilib.schema import File
import os
import sys
import csv
import cv2

import tqdm
from cfg_model import CFGModel
from utils.video import VideoWrapper, VisualisationVideo
from detectron2.config import get_cfg

class stuff_detector():

    cfg_model = CFGModel()
    visualizer = VisualisationVideo(cfg_model)

    def __init__(self, path, target, name, save, fpm):
        wrapper = VideoWrapper(path + name + ".mp4", fpm)

        results = {}
        for c in list(self.cfg_model.stuff_idx):
            results[c] = []

        i = 1
        p = os.path.join(target, name)
        for frame, result in tqdm.tqdm(self.visualizer.run_panoptic_cat(wrapper), total=wrapper.total_frames):
            
            if save:
                if not os.path.exists(p):
                    os.makedirs(p)
                wrapper.writeCategories(frame, result, 1)
                cv2.imwrite(p + "/" + name + "_" + str(i) + ".png", frame)
                for r in result:
                    results[r].append(result[r]['area_percent'])
                i = i + 1

        f = open(p + "/" + name + ".txt", "w")
        f.write(str(results))
        f.close()
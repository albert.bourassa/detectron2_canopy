import datetime
import file_validator
import os
from stuff_detector import stuff_detector

################################################
# Author: Albert Bourassa
# view readme for instructions
################################################

path = "./sample/"
fpm = 60

if __name__ == "__main__":
    
    print(str(datetime.datetime.now(tz=None)) + " Starting file validation\n")
    files = file_validator.validate(path)
    targetDir = os.path.join(path, "target")
    if not os.path.exists(targetDir):
        os.makedirs(targetDir)

    print(str(datetime.datetime.now(tz=None)) + " Starting stuff detection\n")
    for f in files:
        sd = stuff_detector(path, targetDir, f, True, fpm)